import gulp from 'gulp';
import eslint from 'gulp-eslint';
import babel from 'gulp-babel';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';
import del from 'del';

const SRC_FILES = 'src/**/*.js';
const OUTPUT_DIR = 'build';

gulp.task('clean', function () {
    return del([OUTPUT_DIR]);
});

gulp.task('lint', function() {
    return gulp.src([SRC_FILES])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

function build(minify) {
    let pipeline = gulp.src([SRC_FILES])
        .pipe(sourcemaps.init())
        .pipe(babel());

    if (minify) {
        pipeline.pipe(uglify());
    }

    return pipeline
        .pipe(sourcemaps.write('.', {
            sourceRoot: function(file) {
                return file.cwd + '/src';
            }
        }))
        .pipe(gulp.dest(OUTPUT_DIR));
}

gulp.task('build', ['lint'], build.bind(null, true));

gulp.task('build:dev', ['lint'], build.bind(null, false));

gulp.task('default', ['build:dev']);
