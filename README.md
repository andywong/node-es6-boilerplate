## node-es6-boilerplate

Boilerplate for Node.js ES6 project.

### Features:

* ES6 using Babel
* Build automation using gulp
* Visual Studio Code integration
* Unit testing (TODO)
* npm tasks (TODO)

### Using the template

#### Checkout the repo

```shell
git clone https://bitbucket.org/andywong/node-es6-boilerplate.git
cd node-es6-boilerplate
```

#### Install dependencies

Ensure node & npm is installed, then install all the project dependencies

```shell
npm -g install gulp
npm install -g typings

npm install
typings install
```

#### Start using the boilerplate in VS Code

* Open project in VS Code
* Open command panel and type “task” to see available gulp tasks
* Enjoy coding!


